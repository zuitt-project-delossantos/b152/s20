let jsonSample = `{
	"sampleKey1": "valueA",
	"sampleKey2": "valueB",
	"sampleKey3": 1,
	"sampleKey4": true
}`

console.log(typeof jsonSample);

let jsonConvert = JSON.parse(jsonSample);

console.log(typeof jsonConvert);
console.log(jsonConvert);


// JSON is not only used in JS but also in other programming languages.





let batches = [
	{
		batch: 'Batch 152'
	},
	{
		batch: 'Batch 156'
	}

];
// To turn JS Objects into JSON we use the method JSON.stringify().
// This method will return JSON format out of the objects that we pass as an argument.

let batchesJSON = JSON.stringify(batches);
console.log(batchesJSON)




let data = {
	name: "Katniss",
	age: 20,
	address: {
		city: "Kansas City",
		state: "Kansas"
	}
}
let dataJSON = JSON.stringify(data);
console.log(dataJSON)
// JSON.stringify is commonly used when trying to pass data  from one application to another via HTTP requests. HTTP requests are requests for data between the client(browser/page/app) and a server.




/*
	Mini Activity

	Turn the objects in data2,data3,data4 into stringified JSON using the JSON.stringify() method and save them in their apporpriate variables:

	data2JSON = stringified data2
	data3JSON = stringified data3
	data4JSON = stringified data4

	log them in the console.

	Turn the assets array from our previous mini-activity to stringified JSON array and save the resulting JSON in the variable called jsonArray

	log jsonArray in the console.

*/

let data2 = {
	username: "saitamaOPM",
	password: "onepunch",
	isAdmin: true
}

let data3 = {
	username: "lightYagami",
	password: "notKiraDefinitely",
	isAdmin: false
}

let data4 = {
	username: "Llawliett",
	password: "yagamiiskira07",
	isAdmin: false
}

// SOLUTION
let data2JSON = JSON.stringify(data2)
console.log(data2JSON)

let data3JSON = JSON.stringify(data3)
console.log(data3JSON)

let data4JSON = JSON.stringify(data4)
console.log(data4JSON)



let items = `[
{
	"id": "shop-1",
	"name": "Oreos",
	"stock": 5,
	"price": 50
},
{
	"id": "shop-2",
	"name": "Doritos",
	"stock": 10,
	"price": 150
}
]`

// JSON.parse() turns JSON into JS Objects
let itemsJSArr = JSON.parse(items);
console.log(itemsJSArr);

// remove last object:
itemsJSArr.pop();
console.log(itemsJSArr);

// JSON.stringify() the itemsJSArr and re-assign to items:
items = JSON.stringify(itemsJSArr);
console.log(items);

/*
	Server - page/browser (Client)

	-Client sends data in JSON format 
	-Server receives data in JSON format
		-To manipulate/process the sent data
			-parse the JSON back into JS object
		-stringify the data back into JSON
		-send it back to Client (page/browser)
	-Client receives data in JSON format
*/







/*
	Mini Activity


	Remove the last item in the courses JSON array.
		-first turn the courses array into a JS Object and save the result into a variable called coursesJSArr
		-remove the last item in coursesJSArr by this time, coursesJSArr is now a JS Array, we can manipulate with array methods.
		-reassign the value of courses variable with a JSON stringified coursesJSArr

	Add a new item in the courses JSON array.
		-first turn the courses array into JS object and save the result into a variable called coursesJSArr2
		-add a new object to coursesJSArr2 using push() with the same key-value pairs:
			name: <string>
			description: <string>
			price: <num>
		-reassign the value of courses variable with JSON stringified coursesJSArr2
*/
let courses = `[
	{
		"name": "Math 101",
		"description": "learn the basics of Math.",
		"price": 2500
	},
	{
		"name": "Science 101",
		"description": "learn the basics of Science.",
		"price": 2500
	}

]`


// SOLUTION
let coursesJSArr = JSON.parse(courses);
console.log(coursesJSArr)

coursesJSArr.pop();
console.log(coursesJSArr)

courses = JSON.stringify(coursesJSArr);
console.log(courses)

let coursesJSArr2 = JSON.parse(courses);
console.log(coursesJSArr2)

coursesJSArr2.push({
	"name": "English 101",
	"description": "learn the basics of English.",
	"price": 3000
})
console.log(coursesJSArr2)

courses = JSON.stringify(coursesJSArr2);
console.log(courses)