let users = [

{
	username: "mike9000",
	email: "michaeljames@gmail.com",
	password: "mikecutie1999",
	isAdmin: false
},
{
	username: "justin89",
	email: "justintlake@gmail.com",
	password: "iamnsync00",
	isAdmin: true
}
];

// How can we access items in an array of objects?
console.log(users[0]);
console.log(users[1]);

// Log only the "email" of the second object in the array.
console.log(users[1].email);

// Array Methods also work on an array of objects


// What method can we use to ADD NEW OBJECT at the end of the array?

users.push({
	username: "abbeyarcher",
	email: "abbeyarrows@gmail.com",
	password: "bowandabbey",
	isAdmin: false
})
console.log(users);

let newUser = {
	username: "hanna1993",
	email: "hannafight@gmail.com",
	password: "fighting1993",
	isAdmin: false,
}

users.push({
	username: "keane5956",
	email: "keane@gmail.com",
	password: "BabyCharles",
	isAdmin: false,
})
console.log(users)



users.push({
	username: "kyle",
	email: "kyle@gmail.com",
	password: "emerut",
	isAdmin: false,
})
console.log(users)

users.push({
	username: "irish",
	email: "irish@gmail.com",
	password: "pame",
	isAdmin: false,
})
console.log(users)






// ANOTHER WAY OF PUSHING NEW OBJECT

class User {
	constructor(username,email,password){
		this.username = username;
		this.email = email;
		this.password = password;
		this.isAdmin = false;
	}
}

let user1 = new User("kateduchess", "nottherealone@gmail.com", "imnotroyalty");
users.push(user1);

console.log(users);


let user2 = new User("damianlillard","notreallydamian@gmail.com","0damian0");
users.push(user2);
console.log(users)




// register function should be able to push a new user object in our array:

function register(username,email,password){
	// console.log(username);
	// console.log(email);
	// console.log(password);
	users.push({
		username: username,
		email: email,
		password: password,
		isAdmin: false


	})
}

console.log(users)


register("jeffrey1888", "immortal1@gmail.com","awesomemortal1");




// find() is like forEach and map wherein, it will iterate/loop over each item in the array and then return a condition. If the condition returned results to true, the find() method will return the item that is currently iterated or looped over.
// In find(), map(), we loop over every item in the array.

function login(emailInput,pwInput){
	console.log(emailInput);
	console.log(pwInput);

	let foundUser = users.find((user)=>{
		// console.log(user.email);
		// console.log(user.password);

		return user.email === emailInput && user.password === pwInput;
	})
	// console.log(foundUser);

	if(foundUser !== undefined){
		console.log("Thank you for logging in.")
	} else {
		console.log("Invalid Credentials. No User Found.")
	}
}

login("immortal1@gmail.com", "awesomemortal1");